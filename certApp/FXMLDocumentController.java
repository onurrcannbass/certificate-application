package certapp;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javax.imageio.ImageIO;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Onur Can BAŞ
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private Button tempButton;
    @FXML
    private Button listButton;
    @FXML
    private Button runButton;
    @FXML
    private Button tryButton;
    @FXML
    private ImageView imagePreview;
    @FXML
    private TextField heightText;
    @FXML
    private TextField widthText;

    int height;
    int width;
    int x = 0;
    Image tempp;

    BufferedImage bufTempp;
    File f1, f2;
    List<String> nameList = new ArrayList();
    @FXML
    private Label fontSizeLabel;
    @FXML
    private TextField fontSizeText;
    @FXML
    private ComboBox<String> formatCombo;
    @FXML
    private ComboBox<String> colorCombo;

    @FXML

    private void tempButtonAction(ActionEvent event) throws IOException {
        FileChooser tempChooser = new FileChooser();
        tempChooser.getExtensionFilters().add(new ExtensionFilter("JPG", "*.jpg"));
        f1 = tempChooser.showOpenDialog(null);

        if (f1 != null) {

            bufTempp = ImageIO.read(f1);
            tempp = new Image(f1.toURI().toString());
            imagePreview.setImage(tempp);
        } else {
            tempp = null;
        }

    }

    @FXML
    private void listButtonAction(ActionEvent event) throws FileNotFoundException, IOException {
        {
            FileChooser listChooser = new FileChooser();
            listChooser.getExtensionFilters().add(new ExtensionFilter("TXT", "*.txt"));
            f2 = listChooser.showOpenDialog(null);
            try {

                Scanner myReader = new Scanner(f2);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    nameList.add(data);

                }
                myReader.close();
            } catch (FileNotFoundException e) {
                System.out.println("An error occurred.");
            }
        }
    }

    @FXML
    private void tryButtonAction(ActionEvent event) throws IOException {
        height = Integer.parseInt(heightText.getText());
        width = Integer.parseInt(widthText.getText());
        bufTempp = ImageIO.read(f1);
        bufTempp = process(bufTempp, width, height, "@onurrcannbass", fontSizeText.getText());
        imagePreview.setImage(convertToFxImage(bufTempp));
    }

    @FXML
    private void runButtonAction(ActionEvent event) throws IOException, InterruptedException {

        height = Integer.parseInt(heightText.getText());
        width = Integer.parseInt(widthText.getText());

        DirectoryChooser dc = new DirectoryChooser();
        File f = dc.showDialog(null);

        for (int i = 0; i < nameList.size(); i++) {
            bufTempp = ImageIO.read(f1);

            bufTempp = process(bufTempp, width, height, nameList.get(i), fontSizeText.getText());
            

            String nameENG = nameList.get(i).replace("İ", "I");
            nameENG = nameENG.replace("ı", "i");
            nameENG = nameENG.replace("ö", "o");
            nameENG = nameENG.replace("Ö", "O");
            nameENG = nameENG.replace("ü", "u");
            nameENG = nameENG.replace("Ü", "U");
            nameENG = nameENG.replace("ğ", "g");
            nameENG = nameENG.replace("Ğ", "G");
            nameENG = nameENG.replace("ş", "s");
            nameENG = nameENG.replace("Ş", "S");
            nameENG = nameENG.replace("ç", "c");
            nameENG = nameENG.replace("Ç", "C");
            File outputFile = new File(f.getAbsolutePath() + "\\" + nameENG + ".jpg");
            ImageIO.write(bufTempp, "jpg", outputFile);
        }

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Sonuç");
        String s = "Başarıyla Sonuçlandı";
        alert.setContentText(s);

        Optional<ButtonType> result = alert.showAndWait();
    }

    private BufferedImage process(BufferedImage old, int x, int y, String s, String fontsize) {

        int w = old.getWidth();
        int h = old.getHeight();
        BufferedImage img = new BufferedImage(
                w, h, BufferedImage.TYPE_INT_RGB);
        int[] rgbs = old.getRGB(0, 0, w, h, null, 0, w);

        Graphics2D g2d = img.createGraphics();
        g2d.drawImage(old, 0, 0, w, h, null);
        if (colorCombo.getSelectionModel().getSelectedItem().equals("Black")) {
            g2d.setPaint(Color.BLACK);
        } else {
            g2d.setPaint(Color.WHITE);
        }
        img.setRGB(0, 0, w, h, rgbs, 0, w);
        if (formatCombo.getSelectionModel().getSelectedItem().equalsIgnoreCase("serif")) {
            g2d.setFont(new Font("Serif", Font.BOLD, Integer.parseInt(fontsize)));
        } else {
            g2d.setFont(new Font("Great Vibes",Font.TRUETYPE_FONT,Integer.parseInt(fontsize)));

        }
        FontMetrics fm = g2d.getFontMetrics();
        int stringLen = (int) g2d.getFontMetrics().getStringBounds(s, g2d).getWidth();
        int start = width / 2 - stringLen / 2;
        g2d.drawString(s, start + x, y);

        g2d.dispose();
        return img;
    }

    private static Image convertToFxImage(BufferedImage image) {
        WritableImage wr = null;
        if (image != null) {
            wr = new WritableImage(image.getWidth(), image.getHeight());
            PixelWriter pw = wr.getPixelWriter();
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    pw.setArgb(x, y, image.getRGB(x, y));
                }
            }
        }

        return new ImageView(wr).getImage();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        colorCombo.getItems().removeAll(colorCombo.getItems());
        colorCombo.getItems().addAll("Black", "White");
        colorCombo.getSelectionModel().select("Black");

        formatCombo.getItems().removeAll(formatCombo.getItems());
        formatCombo.getItems().addAll("Serif","Great Vibes");
        formatCombo.getSelectionModel().select("Serif");
    }

}
